import axios from "axios";

export const axiosInstance = axios.create({
});
export const fetchAxios = async (data) => {
    try {
        const response = await axiosInstance(data.url, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            },
            params: data.params
        })
        return response;
    } catch (error) {
        throw error;
    }
}
