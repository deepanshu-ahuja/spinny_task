import React, { useEffect, useState } from "react";
import { searchAction } from "../../store/actions/spinnyAction";
import { connect } from "react-redux";
import { axiosInstance as axios } from './../../axios/axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import classes from "./spinny.module.css"

const Spinny = (props) => {

    const [q, setQ] = useState("");
    const [limit, setLimit] = useState(16);
    const [page, setPage] = useState(1);
    let spinner, list = null

    const onSearchSubmit = (event, isReset) => {
        event.preventDefault();
        const params = {
            q: q === "" ? "naruto" : q,
            limit,
            page,
        }
        props.onSearch({ params, resetList: isReset })

    }
    // const onLoadMore = (event, isReset) => {
    //     event.preventDefault();
    //     const params = {
    //         q: q === "" ? "naruto" : q ,
    //         limit,
    //         page,
    //     }
    //     props.onSearch({ params, resetList: isReset })
    // }

    const handleChange = (event) => {
        // event.persist();
        let { name, value } = event.target;
        setQ(value)

    }
    useEffect(() => {
        let params = {
            q: q === "" ? "naruto" : q,
            limit,
            page
        }
        props.onSearch({ params, resetList: true })
    }, [])

    if (props.loading)
        spinner = <p>loading...</p>

    if (props.list)
        if (props.list.length > 0) {
            console.log("if")
            list =

                <div className={`row d-flex ${classes.main}`}>
                    {props.list.map(data => {
                        return (<div className="col-md-4" key={data.mal_id}>
                            <div className={`${classes.gamingSubsec} d-flex flex-column align-items-center`}>
                                <img src={data.image_url} className={classes.imageBox} />
                                <h4>{data.title}</h4>
                            </div>
                        </div>
                        )
                    })
                    }
                </div>
        }
        else {
            console.log("else")
            list = <p>nothing to display</p>
        }
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className={classes.searchContainer}>
                        <form action="">
                            <input type="text" name="q"
                                value={q}
                                onChange={handleChange} />
                            <button type="submit"
                                onClick={(event) => { onSearchSubmit(event, true) }}>GO</button>
                        </form>
                    </div>
                </div>
                {list}
                <div class="d-flex align-items-center justify-content-center">
                    <button
                        onClick={(event) => { onSearchSubmit(event, false) }}
                        class={classes.loadMore}
                        disabled={!props.list || props.list === 0}
                        >LOAD MORE
                        
                        </button>
                </div>
            </div>
        </div>

    );
}


const mapStateToProps = (state) => {
    return {
        list: state.spinny.list,
        loading: state.spinny.loading
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onSearch: (data) => dispatch(searchAction(data)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Spinny);
