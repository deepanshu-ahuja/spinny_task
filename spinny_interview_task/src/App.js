import React from 'react';
import logo from './logo.svg';
import './App.css';
import Spinny from "./Components/Spinny/Spinny"



function App() {
  return (
    <div className="App">
      <Spinny />
    </div>
  );
}

export default App;
