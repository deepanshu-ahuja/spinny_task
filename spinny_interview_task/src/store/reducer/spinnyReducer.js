import * as actionTypes from "../actionTypes"
let initialState = {
    loading: false,
    list: []
};

const spinny = (state = initialState, action) => {

    console.log("Action", action)
    switch (action.type) {

        case actionTypes.SPINNY_START_SEARCH: {
            return { ...state, loading: true }
        }

        case actionTypes.SPINNY_SUCCESS_SEARCH: {

            let updatedSearchList = [];
            if (action.data.resetList)
                updatedSearchList = [...action.data.searchList];
            else {
                let prevSearchList = [...state["list"]];
                updatedSearchList = [...prevSearchList, ...action.data.searchList];
            }

            return { ...state, list: updatedSearchList, ...action.data.metaData, loading: false }
        }

        case actionTypes.SPINNY_FAIL_SEARCH: {
            return { ...state, loading: false }
        }

        default:
            return state;
    }
};

export default spinny;
