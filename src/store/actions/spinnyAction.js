
import { axiosInstance, fetchAxios } from './../../axios/axios';
import * as actionTypes from "../actionTypes"

export const initSearch = () => {
    return {
        type: actionTypes.SPINNY_START_SEARCH
    }
}


export const onFetchSuccess = (data) => {
    return {
        type: actionTypes.SPINNY_SUCCESS_SEARCH,
        data
    }
}

export const onFetchFailure = (data) => {
    return {
        type: actionTypes.SPINNY_FAIL_SEARCH
    }
}

export const searchAction = (data) => {
    return async (dispatch) => {
        try {
            dispatch(initSearch())

            const response = await fetchAxios({
                url: `https://api.jikan.moe/v3/search/anime`,
                params: { ...data.params }
            })
            if (response.status === 200 && response.data) {
                let searchList = [...response.data.results]
                delete response.data.results
                dispatch(onFetchSuccess({ searchList, metaData: response.data, resetList: data.resetList }))
            }
            else
                dispatch(onFetchFailure())
        }
        catch (err) {
            dispatch(onFetchFailure())
        }

    }
}